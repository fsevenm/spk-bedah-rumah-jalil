<!DOCTYPE html>
<html lang="en">

<?php 
    include 'head.php';
    include "koneksi.php";
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <?php include 'menu.php'; ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->


                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->


                        <!-- Earnings (Monthly) Card Example -->


                        <!-- Earnings (Monthly) Card Example -->

                    </div>
                </div>

                <!-- Content Row -->

                <div class="panel-body">
                    <!-- Tab panes -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800"></h1>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">

                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">DATA KRITERIA</h6>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <a class="btn btn-primary ml-4 mt-4" href="kriteriatambah.php">
                                        <span class="text">Tambah Kriteria</span>
                                    </a>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>KRITERIA</th>
                                                <th>BOBOT</th>

                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                                $sql = $koneksi->query('SELECT * FROM tab_kriteria');
                                                while ($row = $sql->fetch_array()) {
                                            ?>
                                            <tr>
                                                <td><?php echo $row[0] ?></td>
                                                <td><?php echo $row[1] ?></td>
                                                <td><?php echo $row[2] ?></td>

                                                <td align="center"><a title="Edit" class="text-info"
                                                        href="editkriteria.php?id_kriteria=<?php echo $row['0'] ?>"><i
                                                            class="fa fa-edit fa-fw"></i> </td>
                                                <td align="center"><a title="Hapus" class="text-danger"
                                                        href="hapuskriteria.php?id_kriteria=<?php echo $row['0'] ?>"
                                                        onClick="return warning();"><i class="fa fa-trash fa-fw"></i>
                                                </td>

                                                <td align="center">
                                                    <a href="datasub.php?id_kriteria=<?php echo $row['0'] ?>"
                                                        class="btn btn-primary btn-sm btn-icon-split">
                                                        <span class="icon text-white">
                                                            <i class="fas fa-user"></i>
                                                        </span>
                                                        <span class="text">Data Sub Kriteria</span>
                                                    </a>
                                                </td>

                                            </tr>

                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php include "footer.php" ?>