<!DOCTYPE html>
<html lang="en">

<?php include 'head.php';

include "koneksi.php";
?>



<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>MENU</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Pages Collapse Menu -->

            <?php include 'menu.php';?>


            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Content Row -->
                    <div class="row">

                    </div>
                </div>



                <!-- Content Row -->


                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-9 mb-6">

                        <!-- Project Card Example -->

                    </div>

                    <!-- Color System -->


                </div>

                <div class="col-lg-4 mb-3">

                    <!-- Illustrations -->
                    <div class="card shadow mb-8">
                        <div class="card-header py-3">
                            <a> EDIT SUB-KRITERIA </a>
                        </div>
                        <div class="card-body">

                            <div class="row">

                                <?php
                                    $id_kriteria_sub = $_GET['id_kriteria_sub'];
                                    $sql = "SELECT*FROM tab_sub WHERE id_kriteria_sub='$id_kriteria_sub'";
                                    $hasil = $koneksi->query($sql);
                                    if ($hasil->num_rows > 0) {
                                        $row = $hasil->fetch_row();
                                        $nama = $row[2];
                                        $id_kriteria=$row[1];
                                        $sub=$row[3];
                                        $nilai=$row[4];
                                        //$id=$row[0];


                                    }
                                ?>
                                <form class="form" action="editsub.php" method="post">


                                    <div class="row">
                                        <label class="control-label col-lg-5">Id Sub-Kriteria</label>

                                        <div class="col-lg-5">
                                            <input type="text" class="form-control" id="id_kriteria_sub"
                                                name="id_kriteria_sub" required value="<?=$id_kriteria_sub?>" readonly>
                                        </div>

                                    </div><br>

                                    <div class="row">
                                        <label class="control-label col-lg-5">Id Kriteria</label>

                                        <div class="col-lg-5">
                                            <input type="text" required class="form-control" id="id_kriteria" name="id_kriteria"
                                                value="<?=$id_kriteria?>" readonly>
                                        </div>

                                    </div><br>



                                    <div class="row">
                                        <label class="control-label col-lg-5">Kriteria</label>


                                        <div class="col-lg-5">
                                            <input class="form-control" type="text" required name="nama_kriteria"
                                                value="<?=$nama?>" readonly>
                                        </div>


                                    </div><br>

                                    <div class="row">
                                        <label class="control-label col-lg-5">Sub Kriteria</label>
                                        <div class="col-lg-5">
                                            <input class="form-control" type="text" required name="sub_kriteria"
                                                value="<?=$sub?>">
                                        </div>

                                    </div><br>
                                    <div class="row">
                                        <label class="control-label col-lg-5">Nilai Sub-Kriteria</label>
                                        <div class="col-lg-5">
                                            <input class="form-control" type="text" required name="nilai" value="<?=$nilai?>">
                                        </div>

                                    </div><br>

                                    <div class="form-group">

                                        <div class="form-group">
                                            <input class="btn btn-primary btn-user " type="submit" name="ubah"
                                                value="Ubah Sub-Kriteria">
                                        </div>
                                    </div>

                                </form>

                                <?php 
                    
                                    if (isset($_POST['ubah'])) {
                                    // $first_NIK=$_GET['NIK']; 
                                    
                                    $id_kriteria_sub=$_POST['id_kriteria_sub'];
                                    $id_kriteria=$_POST['id_kriteria'];
                                    $nama=$_POST['nama_kriteria'];
                                    $sub=$_POST['sub_kriteria'];

                                    $nilai=$_POST['nilai'];

                                    // $sql = "UPDATE tab_sub SET id_kriteria_sub='$id_kriteria_sub',id_kriteria='$id_kriteria',
                                        //nama_kriteria='$nama',sub_kriteria='$sub', nilai_sub='$nilai' WHERE id_kriteria_sub='$id_kriteria_sub'";

                                    $sql = "UPDATE tab_sub SET id_kriteria_sub='$id_kriteria_sub',id_kriteria='$id_kriteria',nama_kriteria='$nama',sub_kriteria='$sub',nilai_sub='$nilai' WHERE id_kriteria_sub='$id_kriteria_sub'";

                                    //$sql = "UPDATE tab_kriteria SET id_kriteria='$id_kriteria',
                                    // nama_kriteria='$nama',bobot='$bobot' WHERE id_kriteria='$id_kriteria'";

                                        $hasil=$koneksi->query($sql);
                                        if ($hasil) {
                                        echo "<script>
                                            alert('berhasil di Ubah !');
                                            window.location.href='kriteria.php'; 
                                            </script>";    
                                        }
                                    }
                                ?>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Approach -->

            </div>

            <?php include "footer.php" ?>