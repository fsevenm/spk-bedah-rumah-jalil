<!-- Logout Modal-->
<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Opsi Cetak</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <a target="_blank" href="print.php" class="btn btn-primary mb-3">Cetak Semua</a>
                <form action="print.php" method="get">
                    <div class="form-group">
                        <label for="">Cetak Berdasarkan Dusun</label>
                        <select name="dusun" class="form-control">
                            <?php $sql = $koneksi->query("SELECT DISTINCT Dusun FROM masyarakat ORDER BY Dusun ASC"); ?>
                            <?php while($row = $sql->fetch_array()): ?>
                            <option value="<?= $row[0]; ?>">Dusun <?= $row[0]; ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Cetak" name="button" class="btn btn-primary">
                    </div>
                </form>
                <form action="print.php" method="get">
                    <div class="form-group">
                        <label for="">Cetak Berdasarkan Jenis Kelamin</label>
                        <select name="jenis_kelamin" class="form-control">
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Cetak" name="button" class="btn btn-primary">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>