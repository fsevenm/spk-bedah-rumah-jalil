<!DOCTYPE html>
<html lang="en">

<?php
    include 'head.php';
    include "koneksi.php";
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <!-- Divider -->


            <!-- Heading -->
            <div class="sidebar-heading">

            </div>

            <!-- Nav Item - Pages Collapse Menu -->

            <?php include 'menu.php';?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-4">
                            <div class="d-sm-flex align-items-center justify-content-between mb-2">
                                <h6 class="m-0 font-weight-bold text-primary">DATA IDENTITAS MASAYARAKAT </h6>

                            </div>

                            <!-- Content Row -->
                            <div class="row">
                            </div>
                        </div>


                        <?php
                            //koneksi

                            include "koneksi.php";

                            //pemberian kode id secara otomatis

                        ?>

                        <!-- Illustrations -->
                        <div class="card shadow mb-8">

                            <div class="card-body">

                                <form class="user" action="surveidata.php" method="post">

                                    <?php
                                        $NIK = $_GET['NIK'];
                                        $sql = "SELECT * FROM masyarakat WHERE NIK='$NIK'";
                                        $hasil = $koneksi->query($sql);
                                        if ($hasil->num_rows > 0) {
                                            $row = $hasil->fetch_row();
                                            $Nama = $row[1];
                                            $id_masyarakat = $row[0];

                                            $jk = $row[2];
                                            $desa = $row[3];
                                            $Dusun = $row[4];
                                            // $RT = $row[5];

                                        }
                                    ?>

                                    <div class="row">
                                        <label class="control-label col-lg-3">NIK</label>

                                        <div class="col-lg-3">
                                            <input type="hidden" name="id_alternatif" value="<?= $id_masyarakat; ?>">
                                            <input type="text" class="form-control " id="NIK" name="NIK"
                                                value="<?php echo $NIK ?>" readonly>
                                        </div>

                                    </div><br>

                                    <div class="row">
                                        <label class="control-label col-lg-3">NAMA</label>

                                        <div class="col-lg-4">
                                            <input type="text" name="Nama" value="<?=$Nama?>" class="form-control "
                                                readonly>
                                        </div>

                                    </div><br>

                                    <div class="row">
                                        <label class="control-label col-lg-3">DESA</label>

                                        <div class="col-lg-4">
                                            <input type="text" name="desa" value="<?=$desa?>" class="form-control "
                                                id="exampleInputPassword" readonly>
                                        </div>

                                    </div><br>

                                    <div class="row">
                                        <label class="control-label col-lg-3">DUSUN</label>

                                        <div class="col-lg-4">
                                            <input type="text" name="Dusun" value="<?=$Dusun?> " class="form-control "
                                                id="exampleRepeatPassword" readonly>
                                        </div>

                                    </div><br>

                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary">PENILAIAN MASYARAKAT </h6>
                                        </div>

                                        <table class="table table-bordered" id="dataTable" width="60%" cellspacing="0">
                                            <thead">


                                                <?php
                                                    //$sql ="SELECT * FROM tab_kriteria
                                                    //JOIN tab_sub ON tab_kriteria.id_kriteria=tab_sub.id_kriteria";

                                                    $sql = "SELECT  * FROM tab_kriteria WHERE id_kriteria ORDER BY id_kriteria ASC ";
                                                    $hasil = $koneksi->query($sql);
                                                    if ($hasil->num_rows > 0) {
                                                        while ($ro = $hasil->fetch_row()) {
                                                            $id = $ro['0'];
                                                //$id=$ro['id_kriteria'];
                                                ?>
                                                <tr>

                                                    <div class="row">
                                                        <td><?=$ro[1]?></td>
                                                    </div>

                                                    <td>
                                                        <div class="btn-group">

                                                            <select class="form-control" name="kriteria-<?= $ro[0] ?>">
                                                                <?php

                                                                        $result = mysqli_query($koneksi, "SELECT * FROM tab_sub WHERE id_kriteria=$id ORDER BY sub_kriteria");
                                                                        while ($row = mysqli_fetch_assoc($result)) {

                                                                            echo '<option value="'.$row["nilai_sub"].'">'.$row["sub_kriteria"].'</option>';
                                                                            $nilai = $row[4];
                                                                        }
                                                                    }}
                                                                ?>
                                                            </select>


                                                            <?php ?>
                                                        </div>
                                                    </td>
                                    </div>
                                    </td>
                                    </tr>


                                    </thead>
                                    </table>

                                    <div class="form-group">
                                        <input class="btn btn-primary ml-4 mt-4" type="submit" name="simpan"
                                            value="SIMPAN DATA">
                                    </div>

                                </form>

                                <?php

                                    if (isset($_POST['simpan'])) {

                                        $NIK = $_POST['NIK'];
                                        $id_kriteria = $id;

                                        $nilai = $nilai;

                                        $hapus_semua = "DELETE FROM tab_topsis WHERE NIK = " . $NIK;
                                        $koneksi->query($hapus_semua);

                                        foreach ($_POST as $key => $value) {
                                            if (strpos($key, 'kriteria') !== false) {
                                                $id_kriteria = explode('-', $key)[1];
                                                $nilai = (float) $value;
                                                $masuk = "INSERT INTO tab_topsis VALUES ('" . $NIK . "','" . $id_kriteria . "','" . $nilai . "')";
                                                $buat = $koneksi->query($masuk);
                                            }
                                        }
                                        $ubah = "UPDATE masyarakat SET Status='SUDAH SURVEI' WHERE NIK = " . $NIK;
                                        $koneksi->query($ubah);
                                        echo "<script>alert('Input Data Berhasil') </script>";
                                        echo "<script>window.location.href = \"survei.php\" </script>";

                                    }

                                ?>

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Approach -->

            </div>

            <?php include "footer.php" ?>