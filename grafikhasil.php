<!DOCTYPE html>
<html lang="en">
<?php 
    include 'head.php'; 
    include 'koneksi.php';
    /**
     * DetermineCategory adalah sebuah fungsi yang dibuat secara custom di MySQL 
     * untuk menentukan kategori keputusan berupa Rusak Ringan, Rusak Sedang atau Rusak Berat
     * berdasarkan nilai yang diperoleh dari perhitungan SPK
     * 
     * Untuk melihat fungsi tersebut dapat masuk ke PhpMyAdmin, arahkan ke nama database
     * lalu masuk ke tab Routines atau Routine dalam bahasa, dapat juga diakses pada tree 
     * di bawah nama tabel yang bersangkutan, pilih Functions atau Fungsi dalam bahasa
     */
    $sql = $koneksi->query("SELECT DetermineCategory(hasil.Nilai) AS kategori, COUNT(*) AS jumlah FROM `hasil` GROUP BY kategori ORDER BY jumlah DESC");
    
    $data_grafik_umum = [
        'Rusak Ringan' => 0,
        'Rusak Sedang' => 0,
        'Rusak Berat' => 0
    ];

    while ($item = $sql->fetch_assoc()) {
        $data_grafik_umum[$item['kategori']] = $item['jumlah'];
    }

    $sql = $koneksi->query("SELECT masyarakat.Dusun, DetermineCategory(hasil.Nilai) AS kategori, COUNT(*) AS jumlah FROM `hasil` INNER JOIN masyarakat ON hasil.NIK=masyarakat.NIK GROUP BY kategori, masyarakat.Dusun ORDER BY masyarakat.Dusun ASC, kategori ASC");
    
    $data_per_dusun = [];
    
    while ($item = $sql->fetch_assoc()) {
        $key = trim($item['Dusun']);
        if (!array_key_exists($key, $data_per_dusun))
            $data_per_dusun[$key] = [
                'Rusak Ringan' => 0,
                'Rusak Sedang' => 0,
                'Rusak Berat' => 0
            ];
        $data_per_dusun[$key][trim($item['kategori'])] = $item['jumlah'];
    }

    $sql = $koneksi->query("SELECT masyarakat.Dusun, masyarakat.Jenis_Kelamin AS jenis_kelamin, COUNT(*) AS jumlah FROM `hasil` INNER JOIN masyarakat ON hasil.NIK=masyarakat.NIK GROUP BY masyarakat.Dusun, masyarakat.Jenis_Kelamin ORDER BY masyarakat.Dusun ASC");

    $data_per_dusun_jk = [];

    while ($item = $sql->fetch_assoc()) {
        $key = trim($item['Dusun']);
        if (!array_key_exists($key, $data_per_dusun_jk))
            $data_per_dusun_jk[$key] = [
                'Laki-laki' => 0,
                'Perempuan' => 0,
            ];
        $data_per_dusun_jk[$key][ucfirst(strtolower(trim($item['jenis_kelamin'])))] = $item['jumlah'];
    }
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->

            <!-- Divider -->
            <?php include 'menu.php'; ?>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">GRAFIK PIE HASIL KESELURUHAN</h6>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <?php $total = array_sum(array_values($data_grafik_umum)); ?>
                                    <?php foreach ($data_grafik_umum as $kategori => $jumlah): ?>
                                        — <?= $kategori; ?>: <code><?= $jumlah; ?> KK (<?= round((int) $jumlah/$total*100, 2) ?>%)</code> <br>
                                    <?php endforeach; ?>
                                    <hr>
                                    <div class="chart-pie pt-4">
                                        <canvas id="pie-chart-umum"></canvas>
                                    </div>
                                    <hr>
                                    Keterangan:
                                    <br>
                                    Diagram menggunakan satuan KK
                                    <br>
                                    KK: Kepala Keluarga
                                </div>
                            </div>
                        </div>
                        <div class="col-12">

                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">GRAFIK BAR PER DUSUN</h6>
                                </div>

                                <div class="card-body">

                                    <!-- Looping section -->
                                    <?php foreach ($data_per_dusun as $dusun => $data): ?>
                                        <div class="row">
                                            <div class="col-md-12 mt-4">
                                                <h4 class="text-center">Dusun <?= $dusun ?></h4>
                                            </div>
    
                                            <div class="col-6 mb-2">
                                                <div class="text-center" style="font-weight: bold;">
                                                    Berdasarkan Jenis Kelamin
                                                </div>
                                            </div>
    
                                            <div class="col-6 mb-2">
                                                <div class="text-center" style="font-weight: bold;">
                                                    Berdasarkan Tingkat Kerusakan
                                                </div>
                                            </div>
    
                                            <div class="col-6">
                                                <?php $total = array_sum(array_values($data_per_dusun_jk[$dusun]));?>
                                                <?php foreach ($data_per_dusun_jk[$dusun] as $jk => $jumlah): ?>
                                                    — <?=$jk;?>: <code><?=$jumlah;?> KK (<?=round((int) $jumlah / $total * 100, 2)?>%)</code> <br>
                                                <?php endforeach;?>
                                                <br>
                                                <div class="chart-bar">
                                                    <canvas id="bar-chart-1-dusun-<?= strtolower($dusun) ?>"></canvas>
                                                </div>
                                            </div>
    
                                            <div class="col-6">
                                                <?php $total = array_sum(array_values($data));?>
                                                <?php foreach ($data as $jk => $jumlah): ?>
                                                    — <?=$jk;?>: <code><?=$jumlah;?> KK (<?=round((int) $jumlah / $total * 100, 2)?>%)</code> <br>
                                                <?php endforeach;?>
                                                <div class="chart-bar">
                                                    <canvas id="bar-chart-2-dusun-<?= strtolower($dusun) ?>"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    <?php endforeach; ?>
                                    Keterangan:
                                    <br>
                                    Diagram menggunakan satuan KK
                                    <br>
                                    KK: Kepala Keluarga
                                    <!-- End looping section -->

                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>

            </div>

            <!-- End of Main Content -->
            <?php include "modalprint.php" ?>

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Skripsi &mdash; Rijalil Maqbul | F55114002 &mdash; 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include "ubahpassword.php" ?>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script>
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = 'Nunito',
        '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';

    // Bar Chart
    <?php foreach($data_per_dusun_jk as $dusun => $data): ?>
    var ctx1_<?= strtolower($dusun) ?> = document.getElementById("bar-chart-1-dusun-<?= strtolower($dusun) ?>");
    var myBarChart1_<?= $dusun ?> = new Chart(ctx1_<?= strtolower($dusun) ?>, {
        type: 'bar',
        data: {
            labels: [<?php echo "\"" . implode("\",\"", array_keys($data)) . "\"" ?>],
            datasets: [{
                label: "Jumlah",
                backgroundColor: "#4e73df",
                hoverBackgroundColor: "#2e59d9",
                borderColor: "#4e73df",
                data: [<?php echo "\"" . implode("\",\"", array_values($data)) . "\"" ?>],
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'month'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 6
                    },
                    maxBarThickness: 25,
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: <?= max(array_values($data)) ?>,
                        maxTicksLimit: 5,
                        padding: 10,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value;
                        }
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return datasetLabel + ': ' + tooltipItem.yLabel + " KK";
                    }
                }
            },
        }
    });

    var ctx2_<?= strtolower($dusun) ?> = document.getElementById("bar-chart-2-dusun-<?= strtolower($dusun) ?>");
    var myBarChart2_<?= $dusun ?> = new Chart(ctx2_<?= strtolower($dusun) ?>, {
        type: 'bar',
        data: {
            labels: [<?php echo "\"" . implode("\",\"", array_keys($data_per_dusun[$dusun])) . "\"" ?>],
            datasets: [{
                label: "Jumlah",
                backgroundColor: "#4e73df",
                hoverBackgroundColor: "#2e59d9",
                borderColor: "#4e73df",
                data: [<?php echo "\"" . implode("\",\"", array_values($data_per_dusun[$dusun])) . "\"" ?>],
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'month'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 6
                    },
                    maxBarThickness: 25,
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: <?= max(array_values($data_per_dusun[$dusun])) ?>,
                        maxTicksLimit: 5,
                        padding: 10,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value;
                        }
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return datasetLabel + ': ' + tooltipItem.yLabel + " KK";
                    }
                }
            },
        }
    });

    <?php endforeach; ?>

    // Pie Chart
    var ctx = document.getElementById("pie-chart-umum");
    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: [<?php echo "\"" . implode("\",\"", array_keys($data_grafik_umum)) . "\"" ?>],
            datasets: [{
            data: [<?php echo "\"" . implode("\",\"", array_values($data_grafik_umum)) . "\"" ?>],
            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
            },
            legend: {
            display: false
            },
            cutoutPercentage: 80,
        },
    });
    </script>
</body>

</html>