<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'; 

include 'koneksi.php';

?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">


            <!-- Divider -->
            <?php include 'menu.php'; ?>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">DATA CALON PENERIMA BANTUAN</h6>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <a href="alternatiftambah.php" class="btn btn-primary ml-4 mt-4">
                                    <span class="text">TAMBAH DATA</span>
                                </a>
                                <a href="" onclick="return confirm('Apakah Anda ingin menghapus semua data?')"
                                    class="btn btn-danger ml-2 mt-4">
                                    <span class="text">HAPUS SEMUA</span>
                                </a>
                            </div>
                            <div class="col-md-3">

                            </div>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table dataTable table-bordered" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIK</th>
                                            <th>NAMA</th>
                                            <th>JENIS KELAMIN</th>
                                            <th>DESA</th>
                                            <th>DUSUN</th>

                                            <th>AKSI</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>NIK</th>
                                            <th>NAMA</th>
                                            <th>JENIS KELAMIN</th>
                                            <th>DESA</th>
                                            <th>DUSUN</th>

                                            <th>AKSI</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        $no=1;
                            $sql = $koneksi->query('SELECT * FROM masyarakat');
                            while ($row = $sql->fetch_array()) {
                                ?>
                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row[0] ?></td>
                                            <td><?php echo $row[1] ?></td>
                                            <td><?php echo $row[2] ?></td>
                                            <td><?php echo $row[3] ?></td>
                                            <td><?php echo $row[4] ?></td>

                                            <td align="center"><a title="Edit" class="text-info"
                                                    href="ubahalternatif.php?NIK=<?php echo $row['0'] ?>"><i
                                                        class="fa fa-edit fa-fw"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                    title="hapus" class="text-danger"
                                                    href="Hapusalternatif.php?NIK=<?php echo $row['0'] ?>"
                                                    onClick="return warning();"><i class="fa fa-trash fa-fw"></i></a>
                                            </td>
                                        </tr>

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php include "footer.php" ?>