<!-- Nav Item - Dashboard -->
<li class="nav-item ">
    <a class="nav-link" href="home.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Halaman Utama</span></a>
    <hr class="sidebar-divider">
</li>
<div class="sidebar-heading">
    MENU
</div>

<!-- Master Data -->
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
        aria-controls="collapseTwo">
        <i class="fas fa-fw fa-cog"></i>
        <span>Master Data</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data</h6>
            
            <?php if ($_SESSION['level'] == 'kabid'): ?>
                <a class="collapse-item" href="kriteria.php">Kriteria</a>
            <?php endif; ?>

            <a class="collapse-item" href="dataalternatif.php">Masyarakat</a>
        </div>
    </div>
</li>

<!-- Menu Survei Masyarakat -->
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Survei Masyarakat</span>
    </a>
    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            
            <a class="collapse-item" href="survei.php">Survei</a>
            
            <a class="collapse-item" href="proses.php">Proses Hasil</a>
            
        </div>
    </div>
</li>

<!-- Hasil Keputusan -->
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
        aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Hasil Keputusan</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="hasilkeputusan.php">Hasil</a>
        </div>
    </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">
<!-- Heading -->
<div class="sidebar-heading">
    Keluar
</div>
<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
    <a class="nav-link" onclick="return confirm('Apakah Anda yakin akan keluar?')" href="logout.php">
        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-1 "></i>
        <span>Logout</span></a>
</li>