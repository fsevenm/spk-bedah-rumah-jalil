<!DOCTYPE html>
<html lang="en">

<?php 
    include 'head.php';
    include ("koneksi.php");
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <?php include 'menu.php'; ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">

                    </div>

                    <!-- Content Row -->
                    <div class="row">

                    </div>
                </div>


                <?php
                    //koneksi
                    include ("koneksi.php");
                    //pemberian kode id secara otomatis
                ?>

                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-6 mb-4">

                        <!-- Project Card Example -->

                    </div>

                    <!-- Color System -->

                </div>

                <div class="col-lg-5 mb-6">

                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Tambah Data Masyarakat</h6>
                        </div>
                        <div class="card-body">

                            <div class="row">

                                <form class="user" action="alternatiftambah.php" method="post">
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="number" required name="NIK" class="form-control " id="exampleFirstName"
                                                placeholder="NIK">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" required name="Nama" class="form-control " id="exampleLastName"
                                                placeholder="Nama Lengkap">
                                        </div>
                                    </div>
                                    <div class="form-group">


                                        <select class=" form-control" name="Jenis_Kelamin" placeholder="Jenis_Kelamin">
                                            <option>Laki-laki</option>
                                            <option>Perempuan</option>
                                        </select>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <select name="desa" required class="form-control">
                                                <option value="Balane">Balane</option>
                                                <option value="Bolobia">Bolobia</option>
                                                <option value="Daenggune">Daenggune</option>
                                                <option value="Doda">Doda</option>
                                                <option value="Kalora">Kalora</option>
                                                <option value="Kanuna">Kanuna</option>
                                                <option value="Kayumpia">Kayumpia</option>
                                                <option value="Porame">Porame</option>
                                                <option value="Rondingo">Rondingo</option>
                                                <option value="Uwemanje">Uwemanje</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" required name="Dusun" class="form-control "
                                                id="exampleRepeatPassword" placeholder="Dusun">
                                        </div>

                                    </div>

                                    
                                    <div class="form-group">
                                        <input class="btn btn-primary " type="submit" name="simpan"
                                            value="Tambah Data">
                                    </div>
                                </form>

                                <?php 
                                    if (isset($_POST['simpan'])) {
                                    $NIK = trim($_POST['NIK']);
                                    $Nama = trim($_POST['Nama']);
                                    

                                    $jk=$_POST['Jenis_Kelamin'];
                                    $desa=$_POST['desa'];
                                    $Dusun=trim($_POST['Dusun']);
                                    
                                    
                                    if ($NIK=="")  {
                                        echo "<script>
                                        alert('masih ada data yang kosong !');
                                        </script>";
                                    }

                                    if ($Nama=="") {

                                    echo "<script>
                                        alert('Data Nama kosong !');
                                        </script>";
                                    }

                                    if ($jk=="") {

                                    echo "<script>
                                        alert('Data jenis_kelamin kosong !');
                                        </script>";
                                    }

                                    if ($Dusun=="") {

                                    echo "<script>
                                        alert('Data Dusun kosong !');
                                        </script>";
                                    }

                                    else{
                                    $sql = "SELECT*FROM Masyarakat WHERE NIK='$NIK'";
                                    $hasil=$koneksi->query($sql);
                                    if ($hasil->num_rows>0) {
                                    $row = $hasil->fetch_row();
                                        echo "<script>
                                        alert('id $NIK sudah ada !');
                                        </script>";
                                    }else{
                                        $sql = "INSERT INTO masyarakat(NIK,Nama,jenis_kelamin,desa,Dusun,Status)
                                        values ('".$NIK."','".$Nama."','".$jk."','".$desa."','".$Dusun."','".$Status='BELUM SURVEI'."')";
                                        $hasil=$koneksi->query($sql);
                                        echo "<script>
                                        alert('Data Berhasil di inputkan !');
                                    window.location.href='dataalternatif.php';
                                        </script>";
                                        // include 'Datamasyarakat.php';
                                    // header("Location: Datamasyarakat.php");
                                    }}}
                                ?>

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Approach -->

            </div>
            
            <?php include "footer.php" ?>