<!DOCTYPE html>
<html lang="en">

<?php 
    include 'head.php';
    include "koneksi.php";
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->

            <!-- Nav Item - Pages Collapse Menu -->
            <?php include 'menu.php'; ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->


                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->


                        <!-- Earnings (Monthly) Card Example -->


                        <!-- Earnings (Monthly) Card Example -->

                    </div>
                </div>

                <!-- Content Row -->


                <!-- Content Row -->

                <div class="panel-body">
                    <!-- Tab panes -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800"></h1>

                        <!-- DataTales Example -->
                        
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">SURVEI DATA MASYARAKAT </h6>
                        </div>

 <?php
$id_kriteria = $_GET['id_kriteria'];
//$sql = "SELECT*FROM tab_kriteria WHERE id_kriteria='$id_kriteria'";

 $sql = "SELECT*FROM tab_kriteria WHERE id_kriteria='$id_kriteria'";
  $hasil=$koneksi->query($sql);
  if ($hasil->num_rows>0) {
  $row = $hasil->fetch_row();
   // $nama=$row[1];
    //$bobot=$row[2];
   
}

//$sql = $koneksi->query("SELECT * FROM tab_sub WHERE id_kriteria='$id_kriteria'");
?>



                         
                                        
                            
                            <div class="card-body">

                                <a class="btn btn-primary mb-4"
                                                        href="tambahsub.php?id_kriteria=<?php echo $row['0'] ?>"> TAMBAH SUB KRITERIA</td>
                                                    </a>

                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>ID KRITERIA</th>
                                                <th>KRITERIA</th>
                                                <th>SUB KRITERIA</th>
                                                <th>NILAI</th>

                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
//$id_kriteria = $_GET['id_kriteria'];
//$sql = "SELECT*FROM tab_kriteria WHERE id_kriteria='$id_kriteria'";

$sql = $koneksi->query("SELECT * FROM tab_sub WHERE id_kriteria='$id_kriteria'");




while ($row = $sql->fetch_array()) {
    ?>
                                            <tr>
                                                <td><?php echo $row[1] ?></td>
                                                <td><?php echo $row[2] ?></td>
                                                <td><?php echo $row[3] ?></td>
                                                <td><?php echo $row[4] ?></td>



                                                <td align="center"><a title="Edit" class="text-info"
                                                        href="editsub.php?id_kriteria_sub=<?php echo $row['0'] ?>"><i
                                                            class="fa fa-edit fa-fw"></i> </td>
                                                <td align="center"><a title="Hapus" class="text-danger" href="hapus_sub.php?id_kriteria_sub=<?php echo $row['0'] ?>" onClick="return warning();" ><i
                                                            class="fa fa-trash fa-fw"></i> </td>

                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>



                                <!-- Approach -->

                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php include "footer.php" ?>