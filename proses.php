<!DOCTYPE html>
<html lang="en">

<?php 
    include 'koneksi.php';
    include 'head.php'; 
    //$awal = microtime(true);
    //$awalmemory = memory_get_usage();
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <!-- Divider -->
            <?php include 'menu.php'; ?>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <?php

                    $tampil = $koneksi->query("SELECT b.Nama,c.nama_kriteria,a.nilai,c.bobot,b.NIK
                        FROM
                            tab_topsis  a 
                            JOIN
                            masyarakat b USING(NIK)
                            JOIN
                            tab_kriteria c USING(id_kriteria) ");

                    $data      =array();
                    $kriterias =array();
                    $bobot     =array();
                    $nilai_kuadrat =array();

                    if ($tampil) {
                    while($row=$tampil->fetch_object()){

                        if(!isset($data[$row->NIK])){
                        $data[$row->NIK]=array();
                        }
                        if(!isset($data[$row->NIK][$row->nama_kriteria])){
                        $data[$row->NIK][$row->nama_kriteria]=array();
                        }
                        if(!isset($nilai_kuadrat[$row->nama_kriteria])){
                        $nilai_kuadrat[$row->nama_kriteria]=0;
                        }
                        $bobot[$row->nama_kriteria]=$row->bobot;
                        $data[$row->NIK][$row->nama_kriteria]=$row->nilai;
                        $nilai_kuadrat[$row->nama_kriteria]+=pow($row->nilai,2);
                        $kriterias[]=$row->nama_kriteria;
                    }
                    }

                    $kriteria     =array_unique($kriterias);
                    $jml_kriteria =count($kriteria);
                ?>


                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">PROSES KEPUTUSAN </h6>
                        </div>

                        <!-- DataTales Example -->
                        <div class="container">
                            <!--container-->
                            <div class="row">
                                <div class="col-lg-2 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <?php 
                                            $awal = microtime(true);
                                            $awalmemory = memory_get_usage();
                                        ?>
                                        <div class="panel-heading">
                                            Evaluation Matrix (x<sub>ij</sub>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th rowspan='3'>No</th>
                                                        <th rowspan='3'>Alternatif</th>
                                                        <th rowspan='3'>Nik</th>

                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        foreach($kriteria as $k)
                                                            echo "<th>$k</th>\n";
                                                        ?>
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $i=0;
                                                        foreach($data as $NIK=>$krit){
                                                            echo "<tr>
                                                            <td>".(++$i)."</td>
                                                            <th>A$i</th>
                                                            <td>$NIK</td>";
                                                            foreach($kriteria as $k){
                                                            echo "<td align='center'>$krit[$k]</td>";
                                                            }
                                                            echo "</tr>\n";
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Rating Kinerja Ternormalisasi (r<sub>ij</sub>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th rowspan='3'>No</th>
                                                        <th rowspan='3'>Alternatif</th>
                                                        <th rowspan='3'>NIK</th>
                                                        <th colspan='<?php echo $jml_kriteria;?>'>Kriteria</th>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                            foreach($kriteria as $k)
                                                            echo "<th>$k</th>\n";
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                            for($n=1;$n<=$jml_kriteria;$n++)
                                                            echo "<th>K$n</th>";
                                                        ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $i=0;
                                                        foreach($data as $NIK=>$krit){
                                                            echo "<tr>
                                                            <td>".(++$i)."</td>
                                                            <th>A{$i}</th>
                                                            <td>{$NIK}</td>";
                                                            foreach($kriteria as $k){
                                                            echo "<td align='center'>".round(($krit[$k]
                                                                /sqrt($nilai_kuadrat[$k])),4)."</td>";
                                                            }
                                                            echo
                                                            "</tr>\n";
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Rating Bobot Ternormalisasi(y<sub>ij</sub>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th rowspan='3'>No</th>
                                                        <th rowspan='3'>Alternatif</th>
                                                        <th rowspan='3'>NIK</th>
                                                        <th colspan='<?php echo $jml_kriteria;?>'>Kriteria</th>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                            foreach($kriteria as $k)
                                                            echo "<th>$k</th>\n";
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                            for($n=1;$n<=$jml_kriteria;$n++)
                                                            echo "<th>K$n</th>";
                                                        ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $i=0;
                                                        $y=array();
                                                        foreach($data as $NIK=>$krit){
                                                            echo "<tr>
                                                            <td>".(++$i)."</td>
                                                            <th>A{$i}</th>
                                                            <td>{$NIK}</td>";
                                                            foreach($kriteria as $k){
                                                            $y[$k][$i-1]=round((($krit[$k]/sqrt($nilai_kuadrat[$k]))/100),9)*$bobot[$k];
                                                            echo "<td align='center'>".$y[$k][$i-1]."</td>";
                                                            }
                                                            echo
                                                            "</tr>\n";
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Solusi Ideal positif (A<sup>+</sup>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th colspan='<?php echo $jml_kriteria;?>'>Kriteria</th>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        foreach($kriteria as $k)
                                                        echo "<th>$k</th>\n";
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                for($n=1;$n<=$jml_kriteria;$n++)
                                                echo "<th>y<sub>{$n}</sub><sup>+</sup></th>";
                                                ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <?php
                    $yplus=array();
                    foreach($kriteria as $k){
                      $yplus[$k]=([$k]?max($y[$k]):min($y[$k]));
                      echo "<th>$yplus[$k]</th>";
                    }
                    ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Solusi Ideal negatif (A<sup>-</sup>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th colspan='<?php echo $jml_kriteria;?>'>Kriteria</th>
                                                    </tr>
                                                    <tr>
                                                        <?php
                    foreach($kriteria as $k)
                      echo "<th>{$k}</th>\n";
                    ?>
                                                    </tr>
                                                    <tr>
                                                        <?php
                    for($n=1;$n<=$jml_kriteria;$n++)
                      echo "<th>y<sub>{$n}</sub><sup>-</sup></th>";
                    ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <?php
                    $ymin=array();
                    foreach($kriteria as $k){
                      $ymin[$k]=[$k]?min($y[$k]):max($y[$k]);
                      echo "<th>{$ymin[$k]}</th>";
                    }

                    ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Jarak positif (D<sub>i</sub><sup>+</sup>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Alternatif</th>
                                                        <th>NIK</th>
                                                        <th>D<suo>+</sup></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                  $i=0;
                  $dplus=array();
                  foreach($data as $nama=>$krit){
                    echo "<tr>
                      <td>".(++$i)."</td>
                      <th>A{$i}</th>
                      <td>{$NIK}</td>";
                    foreach($kriteria as $k){
                      if(!isset($dplus[$i-1])) $dplus[$i-1]=0;
                      $dplus[$i-1]+=pow($yplus[$k]-$y[$k][$i-1],2);
                    }
                    echo "<td>".round(sqrt($dplus[$i-1]),9)."</td>
                     </tr>\n";
                  }
                  ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Jarak negatif (D<sub>i</sub><sup>-</sup>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Alternatif</th>
                                                        <th>NIK</th>
                                                        <th>D<suo>-</sup></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                  $i=0;
                  $dmin=array();
                  foreach($data as $NIK=>$krit){
                    echo "<tr>
                      <td>".(++$i)."</td>
                      <th>A{$i}</th>
                      <td>{$NIK}</td>";
                    foreach($kriteria as $k){
                      if(!isset($dmin[$i-1]))$dmin[$i-1]=0;
                      $dmin[$i-1]+=pow($ymin[$k]-$y[$k][$i-1],2);
                    }
                    echo "<td>".round(sqrt($dmin[$i-1]),9)."</td>
                     </tr>\n";
                  }
                  ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Nilai Preferensi(V<sub>i</sub>)
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Alternatif</th>
                                                        <th>NIK</th>
                                                        <th>V<sub>i</sub></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                  $i=0;
                  $V=array();
                  foreach($data as $NIK=>$krit){
                    echo "<tr>
                      <td>".(++$i)."</td>
                      <th>A{$i}</th>
                      <td>{$NIK}</td>";

                      // echo "<td>".round(sqrt($dmin[$i-1]),9)."</td>
                    foreach($kriteria as $k){
                      $V[$i-1]=(sqrt($dmin[$i-1]))/((sqrt($dmin[$i-1]))+(sqrt($dplus[$i-1])));
                       
                    }
                    echo "<td>{$V[$i-1]}</td></tr>\n";
                   // $a=$V[$i-1];
                   
                   // $nilai=array_values($V);
                    // $nilai1=implode(" ",$a);
 $sql = "SELECT * FROM masyarakat WHERE NIK='$NIK'";
$hasil = $koneksi->query($sql);
if ($hasil->num_rows > 0) {
    $row = $hasil->fetch_row();
    $Nama = $row[1];
    $desa = $row[3];
     $dusun = $row[4];
}
        $hapus_data = "DELETE FROM hasil WHERE NIK=" . $NIK;
        $koneksi->query($hapus_data);
        $masuk = "INSERT INTO hasil VALUES ('".$NIK."','".$Nama."','".$desa."','".$dusun."','".$V[$i-1]."')";
        $buat  = $koneksi->query($masuk);
       
 
                  }
////hitung waktu
$akhirmemory= memory_get_usage();
$akhir = microtime(true);
$totalwaktu = number_format(($akhir  - $awal),4);
$totalmemory= $akhirmemory - $awalmemory;

if ($totalmemory < 1024){
  $memory= $totalmemory."Byte";
} elseif ($totalmemory < 1048576) {
  $memory=round($totalmemory/1024, 4)."Kb";
  # code...
}
else{
  $memory = round($totalmemory/1048576, 3)."Mb";
}
  echo "<p>Waktu Proses Perhitungan adalah:
   ".$totalwaktu." detik, Dengan memory: ".$memory."</p>";
   
                  ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--container-->
                        <!--container-->

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <?php include "footer.php" ?>