<!DOCTYPE html>
<html lang="en">

<?php include 'head.php';

include "koneksi.php";
?>



<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->


            <!-- Nav Item - Pages Collapse Menu -->

            <?php include 'menu.php';?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Content Row -->
                    <div class="row">

                    </div>
                </div>

                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-9 mb-6">

                        <!-- Project Card Example -->

                    </div>

                    <!-- Color System -->

                </div>

                <div class="col-lg-4 mb-3">

                    <!-- Illustrations -->
                    <div class="card shadow mb-8">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">TAMBAH SUB-KRITERIA </h6>
                        </div>
                        <div class="card-body">

                            <div class="row">

                                <?php
                                    $id_kriteria = $_GET['id_kriteria'];
                                    $sql = "SELECT*FROM tab_kriteria WHERE id_kriteria='$id_kriteria'";
                                    $hasil = $koneksi->query($sql);
                                    if ($hasil->num_rows > 0) {
                                        $row = $hasil->fetch_row();
                                        $nama = $row[1];

                                    }
                                ?>
                                <form class="form" action="tambahsub.php" method="post">


                                    <div class="row">
                                        <label class="control-label col-lg-5">Id Kriteria</label>

                                        <div class="col-lg-5">
                                            <input type="text" required class="form-control" id="id_kriteria" name="id_kriteria"
                                                value="<?=$id_kriteria?>" readonly>
                                        </div>

                                    </div><br>


                                    <div class="row">
                                        <label class="control-label col-lg-5">Kriteria</label>


                                        <div class="col-lg-5">
                                            <input class="form-control" type="text" required name="nama_kriteria"
                                                value="<?=$nama?>">
                                        </div>


                                    </div><br>

                                    <div class="row">
                                        <label class="control-label col-lg-5">Sub-Kriteria</label>
                                        <div class="col-lg-5">
                                            <input class="form-control" type="text" required name="sub_kriteria">
                                        </div>

                                    </div><br>
                                    <div class="row">
                                        <label class="control-label col-lg-5">Nilai Sub-Kriteria</label>
                                        <div class="col-lg-5">
                                            <input class="form-control" type="text" required name="nilai">
                                        </div>

                                    </div><br>

                                    <div class="form-group">

                                        <div class="form-group">
                                            <input class="btn btn-primary ml-2 mt-1" type="submit"
                                                name="simpan" value="SIMPAN">
                                        </div>
                                    </div>

                                </form>

                                <?php
                                    if (isset($_POST['simpan'])) {
                                        $id_kriteria_sub = $_POST['id_kriteria_sub'];
                                        $id_kriteria = $_POST['id_kriteria'];
                                        $nama = $_POST['nama_kriteria'];
                                        $sub_kriteria = $_POST['sub_kriteria'];
                                        $nilai = $_POST['nilai'];

                                        $sql = "SELECT * FROM tab_kriteria";
                                        $tambah = $koneksi->query($sql);

                                        if ($row = $tambah->fetch_row()) {

                                            $masuk = "INSERT INTO tab_sub VALUES ('" . $id_kriteria_sub . "','" . $id_kriteria . "','" . $nama . "','" . $sub_kriteria . "','" . $nilai . "')";
                                            $buat = $koneksi->query($masuk);

                                            echo "<script>alert('Input Data Berhasil') </script>";
                                            echo "<script>window.location.href = \"kriteria.php\" </script>";
                                        }
                                    }

                                ?>

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Approach -->

            </div>

            <?php include "footer.php" ?>