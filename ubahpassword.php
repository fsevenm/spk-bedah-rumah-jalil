<?php 
    if (isset($_POST['ubah-password'])) {
        // die(var_dump($_POST));
        $password = $_POST['password'];
        $ulang_password = $_POST['ulangi-password'];
        $username = $_SESSION['username'];
        if ($password != $ulang_password) {
            echo "<script>
                alert('Password tidak sama, coba lagi!');
                window.location = window.location.href;
            </script>";
        } else {
            $sql = "UPDATE login SET Password='$password' WHERE Username='$username'";
            $hasil = $koneksi->query($sql);

            if ($hasil) {
                echo "<script>
                    alert('Password berhasil diubah!');
                    window.location.href = window.location.href;
                </script>";
            }
        }
    }

?>
<!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Password</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="password">Password baru</label>
                            <input type="password" required name="password" class="form-control" id="password">
                        </div>
                        <div class="form-group">
                            <label for="ulangi-password">Ulangi password baru</label>
                            <input type="password" required class="form-control" name="ulangi-password" id="ulangi-password">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <input type="submit" value="Ubah password" name="ubah-password" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>