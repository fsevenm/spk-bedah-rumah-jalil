<!DOCTYPE html>
<html lang="en">
<?php 
    include 'head.php'; 
    include 'koneksi.php';
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->

            <!-- Divider -->
            <?php include 'menu.php'; ?>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"> </h1>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">HASIL KEPUTUSAN </h6>
                        </div>

                        <div class="card-body">

                            <a data-toggle="modal" data-target="#printModal"
                                class="text-white btn btn-md btn-primary shadow-sm mb-3" href="javascript:">
                                <i class="fas fa-print"></i> &nbsp;Cetak Laporan
                            </a>
                            <a class="text-white btn btn-md btn-primary shadow-sm mb-3" href="grafikhasil.php">
                                <i class="fas fa-chart-bar"></i> &nbsp;Grafik Hasil
                            </a>

                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIK</th>
                                        <th>NAMA</th>
                                        <th>DESA</th>
                                        <th>DUSUN</th>
                                        <th>NILAI</th>
                                        <th>PERSENTASE</th>
                                        <th>JENIS KERUSAKAN</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        $no=1;
                                        $sql = $koneksi->query("SELECT * FROM hasil ORDER BY nilai DESC ");
                                        while ($row = $sql->fetch_array()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo $row[0] ?></td>
                                        <td><?php echo $row[1] ?></td>
                                        <td><?php echo $row[2] ?></td>
                                        <td><?php echo $row[3] ?></td>
                                        <td><?php echo $row[4] ?></td>
                                        <td><?php echo $kerusakan = round(($row[4] *100), 2) . '%' ?></td>
                                        <td>
                                            <?php 
                                            if ($kerusakan < 30) echo 'Rusak Ringan'; else if ($kerusakan > 30 && $kerusakan < 65) echo 'Rusak Sedang'; else echo 'Rusak Berat';
                                            ?>
                                        </td>

                                    </tr>

                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>

            </div>

            <!-- End of Main Content -->
            <?php include "modalprint.php" ?>
            <?php include "footer.php" ?>