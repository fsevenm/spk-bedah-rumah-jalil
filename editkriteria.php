<!DOCTYPE html>
<html lang="en">

<?php 
    include 'head.php';
    include ("koneksi.php");
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->

            <?php include 'menu.php'; ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->


                        <!-- Earnings (Monthly) Card Example -->


                        <!-- Earnings (Monthly) Card Example -->

                    </div>
                </div>



                <!-- Content Row -->


                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-6 mb-4">

                        <!-- Project Card Example -->

                    </div>

                    <!-- Color System -->


                </div>

                <div class="col-lg-4 mb-3">

                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">

                            <h6 class="m-0 font-weight-bold text-primary"> EDIT KRITERIA</h6>
                        </div>
                        <div class="card-body">

                            <div class="row">


                                <?php 
                                    $id_kriteria=$_GET['id_kriteria'];
                                    $sql = "SELECT*FROM tab_kriteria WHERE id_kriteria='$id_kriteria'";
                                    $hasil=$koneksi->query($sql);
                                    if ($hasil->num_rows>0) {
                                    $row = $hasil->fetch_row();
                                        $nama=$row[1];
                                        $bobot=$row[2];
                                    
                                    }
                                ?>
                                <form class="form" action="editkriteria.php" method="post">
                                    <a> </a>

                                    <div class="form-group">
                                        <input class="form-control" type="text" required name="id_kriteria"
                                            value="<?=$id_kriteria?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" required name="nama_kriteria" value="<?=$nama?>">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" required name="bobot" value="<?=$bobot?>">
                                    </div>
                                    <div class="form-group">

                                        <div class="form-group">
                                            <input class="btn btn-primary btn-user btn-block" type="submit" name="ubah"
                                                value="UBAH DATA">
                                        </div>
                                    </div>

                                </form>

                                <?php 
                                    if (isset($_POST['ubah'])) {
                                    // $first_NIK=$_GET['NIK']; 
                                    
                                    $id_kriteria=$_POST['id_kriteria'];
                                    $nama=$_POST['nama_kriteria'];
                                    
                                    $bobot=$_POST['bobot'];

                                        $sql = "UPDATE tab_kriteria SET id_kriteria='$id_kriteria',
                                        nama_kriteria='$nama',bobot='$bobot' WHERE id_kriteria='$id_kriteria'";
                                        $hasil=$koneksi->query($sql);
                                        if ($hasil) {
                                        echo "<script>
                                            alert('berhasil di Ubah !');
                                            window.location.href='kriteria.php'; 
                                            </script>";
                                        
                                    }}
                                ?>

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Approach -->

            </div>

            <!-- Footer -->
            <?php include "footer.php" ?>