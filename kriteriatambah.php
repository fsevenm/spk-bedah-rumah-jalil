<!DOCTYPE html>
<html lang="en">

<?php 
    include 'head.php';
    include ("koneksi.php");
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->

            <!-- Nav Item - Pages Collapse Menu -->
            <?php include 'menu.php'; ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                    </div>
                </div>


                <?php
                    include ("koneksi.php");

                    //pemberian kode id secara otomatis
                    $carikode = $koneksi->query("SELECT id_kriteria FROM tab_kriteria") or die(mysqli_error());
                    $datakode = $carikode->fetch_array();
                    $jumlah_data = mysqli_num_rows($carikode);

                    if ($datakode) {
                    $nilaikode = substr($jumlah_data[0], 1);
                    $kode = (int) $nilaikode;
                    $kode = $jumlah_data + 1;
                    $kode_otomatis = str_pad($kode, 0, STR_PAD_LEFT);
                    } else {
                    $kode_otomatis = "1";
                    }

                ?>
                <!-- Content Row -->

                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-6 mb-4">

                        <!-- Project Card Example -->

                    </div>

                    <!-- Color System -->


                </div>

                <div class="col-lg-4 mb-3">

                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Tambah Kriteria</h6>
                        </div>
                        <div class="card-body">

                            <div class="row">

                                <form class="form" action="kriteriatambah.php" method="post">
                                    <a> Silahkan masukan Kriteria Baru </a>

                                    <div class="form-group">
                                        <input class="form-control" type="text" required name="id_krit"
                                            value="<?php echo $kode_otomatis; ?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" requiredtype="text" required name="nm_krit"
                                            placeholder="Nama Kriteria">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" required type="text" name="bobot" placeholder="Bobot">
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-primary" type="submit" name="simpan" value="Tambah">
                                    </div>
                                </form>
                                <?php

                                    if (isset($_POST['simpan'])) {
                                    $id_krit  = $_POST['id_krit'];
                                    $kriteria = $_POST['nm_krit'];
                                    $bobot    = $_POST['bobot'];

                                    $sql    = "SELECT * FROM tab_kriteria";
                                    $tambah = $koneksi->query($sql);



                                if ($kriteria=="") {

                                echo "<script>
                                    alert('Kriteria masih kosong !');
                                    </script>";
                                }

                                if ($bobot=="") {

                                echo "<script>
                                    alert('bobot Masih kosong !');
                                    </script>";
                                }

                                else{
                                $sql = "SELECT*FROM tab_kriteria WHERE id_kriteria='$id_krit'";
                                $hasil=$koneksi->query($sql);

                                    if ($row = $tambah->fetch_row()) {

                                        $masuk = "INSERT INTO tab_kriteria VALUES ('".$id_krit."','".$kriteria."','".$bobot."')";
                                        $buat  = $koneksi->query($masuk);

                                        echo "<script>alert('Input Data Berhasil') </script>";
                                        echo "<script>window.location.href = \"kriteria.php\" </script>";
                                    }
                                    }
                                }

                                ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- Footer -->
            <?php include "footer.php" ?>