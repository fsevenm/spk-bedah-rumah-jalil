<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$page_title;?></title>
    <style>
    * {
        margin: 0;
        padding: 0
    }

    body {
        font-family: 'Times New Roman', Times, serif;
        font-size: 12pt;
    }

    /* HEADER */
    .header-container {
        border-bottom: 5px solid black;
        display: flex;
        /* background-color: red; */
        width: 100%;
        /* margin: 10px; */
        margin-top: 30px;
    }

    .header-logo {
        width: 15%;
        float: left;
        text-align: right;
        padding: 10px 0;
        /* background-color: yellow; */
    }

    .header-right-logo {
        width: 15%;
        float: right;
        text-align: left;
        padding: 10px 0;
        /* background-color: red; */
    }

    .header-text {
        width: 70%;
        /* float: left; */
        padding: 20px 0;
        /* background-color: pink; */
        text-align: center;
    }

    @media print {
        .header-text {
            width: 60%;
            padding: 20px 5%;
        }
    }

    /* CONTENT */
    .content {
        margin-top: 40px;
    }

    .content-title {
        text-align: center;
        width: 60%;
        margin: 0 auto;
        margin-bottom: 40px;
        margin-top: 20px;
    }

    .content-title h3 {
        text-decoration: underline;
    }

    .content-sub-title {
        text-align: center;
        margin-bottom: 20px;
        margin-top: 20px;
    }

    .content p {
        margin: 0 auto;
        width: 85%;
    }

    .content img {
        min-width: 475px;
        max-width: 475px;
    }

    /* LOGO */

    .logo {
        width: 100px;
    }

    /* UTILS */
    .text-bold {
        font-weight: bold;
    }

    .text-center {
        text-align: center;
    }

    .text-secondary {
        font-size: 10pt;
        text-align: right;
        padding-top: 10px;
    }

    .w20 {
        width: 50px;
    }

    /* TABLE */
    .table {
        /* background-color: limegreen; */
        margin: 0 auto;
        width: 85%;
    }

    .table-bordered {
        border: 1px solid black;
        border-spacing: 0;
        border-collapse: collapse;
    }

    .table-bordered td,
    .table-bordered th {
        border: 1px solid black;
        border-bottom-width: 0;
        border-left-width: 0;
        padding: 5px;
    }

    .table-numbered th:first-child {
        width: 15px;
    }

    .table-view img {
        max-width: 100%;
    }

    .table-view td:first-child {
        width: 30%;
    }
    </style>
</head>

<body>
    <div class="header-container">
        <div class="header-logo">
            <img src="./sigi.png" alt="" class="logo">
        </div>
        <div class="header-text">
            <h2>PEMERINTAH KABUPATEN SIGI </h2>
            <h1>DINAS PEKERJAAN UMUM DAN PERUMAHAN</h1>
            <h4>Perumahan Kelapa Gading Blok Anggur No. 11-17 Desa Kalukubula
Kec. Sigi Biromaru
</h3>
        </div>
        <div class="header-right-logo">
            <img src="." alt="" class="logo">
        </div>
    </div>

    <div class="content">
        <div class="content-title">
            <h3>LAPORAN DATA WARGA CALON PENERIMA BANTUAN BEDAH RUMAH</h3>
        </div>
        <div class="content-sub-title">
        </div>
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIK</th>
                    <th>NAMA</th>
                    <th>JENIS KELAMIN</th>
                    <th>DESA</th>
                    <th>DUSUN</th>
                    <th>NILAI</th>
                    <th>PERSENTASE</th>
                    <th>JENIS KERUSAKAN</th>

                </tr>
            </thead>

            <tbody>
                <?php
                include "koneksi.php";
                $no = 1;
                if (isset($_GET['dusun'])) {
                    $dusun = $_GET['dusun'];
                    $sql = $koneksi->query("SELECT hasil.*, masyarakat.Jenis_Kelamin FROM hasil INNER JOIN masyarakat ON hasil.NIK=masyarakat.NIK WHERE masyarakat.Dusun='$dusun' ORDER BY hasil.Nilai DESC");
                } else if (isset($_GET['jenis_kelamin'])) {
                    $jenis_kelamin = $_GET['jenis_kelamin'];
                    $sql = $koneksi->query("SELECT hasil.*, masyarakat.Jenis_Kelamin FROM hasil INNER JOIN masyarakat ON hasil.NIK=masyarakat.NIK WHERE masyarakat.Jenis_Kelamin='$jenis_kelamin' ORDER BY hasil.Nilai DESC");
                } else {
                    $sql = $koneksi->query("SELECT hasil.*, masyarakat.Jenis_Kelamin FROM hasil INNER JOIN masyarakat ON hasil.NIK=masyarakat.NIK ORDER BY hasil.Nilai DESC");
                }
                while ($row = $sql->fetch_array()) {?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $row[0] ?></td>
                    <td><?php echo strtoupper($row[1]) ?></td>
                    <td><?php echo strtoupper($row[5]) ?></td>
                    <td><?php echo $row[2] ?></td>
                    <td><?php echo $row[3] ?></td>
                    <td><?php echo $row[4] ?></td>
                    <td><?php echo $kerusakan = round(($row[4] * 100), 2) . '%' ?></td>
                    <td>
                        <?php
                        if ($kerusakan < 30) {
                            echo 'Rusak Ringan';
                        } else if ($kerusakan > 30 && $kerusakan < 65) {
                            echo 'Rusak Sedang';
                        } else {
                            echo 'Rusak Berat';
                        }?>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <!-- Signature -->
        <div style="float: right; margin-right: 10%;">
            <div style="padding: 20px; width: 200px; text-align: center">
                <div>Mengetahui</div>
                <div>Kepala Seksi</div>
                <div>Pembinaan Perumahan Swadaya</div>
                <br>
                <br>
                <br>
                <div>Azhari Samsuddin, S.T., MM</div>
                <div>Nip. 19801101 201001 1 005</div>
            </div>
        </div>
    </div>

</body>
<script src="vendor/jquery/jquery.min.js"></script>
<script>
$(document).ready(function() {
    window.onload = window.print
});
</script>

</html>