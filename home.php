<!DOCTYPE html>
<html lang="en">

<?php 
    include 'head.php'; 
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>

                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <?php include 'menu.php'; ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>

                <div class="container-fluid">
                    <div class="col-md-6 mt-4">
                        <!-- Illustrations -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Selamat datang,
                                    <?php echo $_SESSION['username']; ?>!</h6>
                            </div>
                            <div class="card-body">
                                <div class="text-center">
                                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                        src="img/undraw_posting_photo.svg" alt="">
                                </div>
                                <p>Sistem Pendukung Keputusan Penerima Bantuan Bedah Rumah Adalah Sistem Yang digunakan
                                    untuk membantu pemerintah, Khususnya Dinas Pekerjaan Umum dan Perumahan Kabupaten
                                    Sigi
                                    dalam membantu mengambil keputusan masyarakat yang lebih diPrioritaskan mendapatkan
                                    bantuan </p>

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Content Row -->

            </div>
        
            <?php include "footer.php" ?>