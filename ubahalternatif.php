<!DOCTYPE html>
<html lang="en">

<?php 
    include 'head.php';
    include ("koneksi.php");
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="homeadmin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-1">SPK BANTUAN BEDAH RUMAH </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->

            <!-- Nav Item - Pages Collapse Menu -->

            <?php include 'menu.php'; ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include "topbar.php" ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Content Row -->
                    <div class="row">

                    </div>
                </div>

                <!-- Content Row -->
                <div class="row">

                    <!-- Content Column -->
                    <div class="col-lg-6 mb-4">

                        <!-- Project Card Example -->

                    </div>

                    <!-- Color System -->

                </div>

                <div class="col-lg-6 mb-6">

                    <!-- Illustrations -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Ubah Data Masayarakat </h6>

                        </div>
                        <div class="card-body">

                            <div class="row">

                                <form class="user" action="ubahalternatif.php" method="post">

                                    <?php 
                                        $NIK = $_GET['NIK'];
                                        $sql = "SELECT*FROM Masyarakat WHERE NIK='$NIK'";
                                        $hasil=$koneksi->query($sql);
                                        if ($hasil->num_rows>0) {
                                        $row = $hasil->fetch_row();
                                        $Nama=$row[1];
                                        

                                        $jk=$row[2];
                                        $desa=$row[3];
                                        $Dusun=$row[4];
                                        //  $RT=$row[5];
                                        
                                        }
                                    ?>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="number" required name="NIK" value="<?=$NIK?>" class="form-control "
                                                id="exampleFirstName">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" required name="Nama" value="<?=$Nama?>" class="form-control "
                                                id="exampleLastName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <select class=" form-control" name="Jenis_Kelamin">
                                            <option <?php if($jk == 'Laki-laki'){ echo 'selected'; } ?>>Laki-laki
                                            </option>
                                            <option <?php if($jk == 'Perempuan'){ echo 'selected'; } ?>>Perempuan
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="text" required name="desa" value="<?=$desa?>" class="form-control "
                                                id="exampleInputPassword">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" required name="Dusun" value="<?=$Dusun?> " class="form-control "
                                                id="exampleRepeatPassword">
                                        </div>

                                    </div>

                                    <hr>
                                    <div class="form-group">
                                        <input class="btn btn-primary" type="submit" name="ubah" value="UBAH DATA">
                                    </div>
                                </form>

                                <?php 
                                    if (isset($_POST['ubah'])) {
                                    // $first_NIK=$_GET['NIK']; 
                                    $NIK=$_POST['NIK'];
                                    $Nama=$_POST['Nama'];
                                    $jk=$_POST['Jenis_Kelamin'];
                                    $desa=$_POST['desa'];
                                    $Dusun=$_POST['Dusun'];
                                    //$RT=$_POST['RTRW'];
                                    
                                    if ($NIK=="")  {
                                        echo "<script>
                                        alert('masih ada data yang kosong !');
                                        </script>";
                                    }

                                    if ($Nama=="") {

                                    echo "<script>
                                        alert('Data Nama kosong !');
                                        </script>";
                                    }

                                    if ($jk=="") {

                                    echo "<script>
                                        alert('Data jenis_kelamin kosong !');
                                        </script>";
                                    }

                                    if ($Dusun=="") {

                                    echo "<script>
                                        alert('Data Dusun kosong !');
                                        </script>";
                                    }
                                    else{
                                        $sql = "UPDATE masyarakat SET NIK='$NIK',
                                        Nama='$Nama',Jenis_Kelamin='$jk',desa='$desa',Dusun='$Dusun' WHERE NIK='$NIK'";
                                        $hasil=$koneksi->query($sql);
                                        if ($hasil) {
                                        echo "<script>
                                            alert('berhasil di Ubah !');
                                            window.location.href='dataalternatif.php'; 
                                            </script>";
                                        }
                                    
                                    }}
                                ?>

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Approach -->

            </div>

            <?php include "footer.php" ?>